package main

import (
	"container/list"
	"fmt"
	"io"
	"strings"
	"unicode"
	"unicode/utf8"
)

// must not be a space before ([) and the adverbs ('\/)
// must be a space before comment (/) and trace (\)
//  +/ is summation,+ /x is a comment
// must be a space between verbs(._) and alphanum
// x _ y is x drop y, x_y is name
// numbers separated by space is a vector of constants
// names and constants separated by space cause application f g x
// there is no notation for lists of length 0 and 1 (except "" and ())
// length 1 lists are deisplayed with leading ','
// verbs will take nouns on the left
// f@+/x is f at sum x whereas f +/x is sum x with initial state f

// strange kdb case
// q).123.a:333
//00:00:00.123
// q)-4!"-0xff"
// "-0xff"
// q)-0xff
// -15f
// q)12:03:123
// 12:03:23
// q)0x
//`byte$()

type stateFn func(*Scanner) stateFn

type Token struct {
	Type int
	Text string
}

// token types

const (
	EOF        = iota
	Error      // 1
	NL         // 2
	Assign     // 3 :
	Char       // 4 printable ascii
	Identifier // 5 alphanum id
	LeftBrack  // 6 [
	LeftParen  // 7 (
	Number     // 8 simple num
	Oper       // 9 known operator
	RightBrack // 10 ]
	RightParen // 11 )
	Semi       // 12 ;
	Space      // 13 ___
	String     // 14 quoted string
	LeftCurly  // 15
	RightCurly // 16
	Sym        // 17
	Comment    // 18
	Temporal   // 19
)

type Scanner struct {
	r          io.ByteReader
	done       bool
	buf        []byte
	tokens     *list.List
	input      string
	leftDelim  string
	rightDelim string
	state      stateFn
	line       int // line number
	pos        int // current pos
	start      int // star pos of current token
	runlen     int // width of current run
}

func (l *Scanner) loadLine() {
	l.buf = l.buf[:0]
	for {
		c, err := l.r.ReadByte()
		if err != nil {
			l.done = true
			break
		}
		if c != '\r' {
			l.buf = append(l.buf, c)
		}
		if c == '\n' {
			break
		}
	}
	l.input = l.input[l.start:l.pos] + string(l.buf)
	l.pos -= l.start
	l.start = 0
}

// main tokeniser func
func (l *Scanner) Next() Token {
	for l.state != nil {
		if l.tokens.Len() > 0 {
			return l.tokens.Remove(l.tokens.Front()).(Token)
		} else {
			l.state = l.state(l)
		}

	}

	return Token{EOF, "EOF"}
}

func NewScanner(r io.ByteReader) *Scanner {
	l := &Scanner{
		r:      r,
		tokens: list.New(),
		state:  lexAny,
		start:  0,
		pos:    0,
		runlen: 0,
	}
	return l

}

const eof = -1

func (l *Scanner) next() rune {
	if !l.done && int(l.pos) == len(l.input) {
		l.loadLine()
	}
	if len(l.input) == int(l.pos) {
		l.runlen = 0
		return eof
	}
	r, w := utf8.DecodeRuneInString(l.input[l.pos:])
	l.runlen = w
	l.pos += w
	return r
}
func (l *Scanner) peek() rune {
	r := l.next()
	l.backup()
	return r
}
func (l *Scanner) backup() {
	l.pos -= l.runlen
}
func (l *Scanner) ignore() {
	l.start = l.pos
}
func (l *Scanner) rewind() {
	l.pos = l.start
	l.runlen = 0
}
func (l *Scanner) emit(t int) {
	if t == NL {
		l.line++
	}
	s := l.input[l.start:l.pos]
	l.tokens.PushBack(Token{t, s})
	l.start = l.pos
	l.runlen = 0
}

func (l *Scanner) accept(valid string) bool {
	if strings.IndexRune(valid, l.next()) >= 0 {
		return true
	}
	l.backup()
	return false
}

func (l *Scanner) acceptRun(valid string) int {
	accepted := 0
	for strings.IndexRune(valid, l.next()) >= 0 {
		accepted++
	}
	l.backup()
	return accepted
}

func (l *Scanner) errorf(format string, args ...interface{}) stateFn {
	l.tokens.PushBack(Token{Error, fmt.Sprintf(format, args)})
	return lexAny
}

func lexAny(l *Scanner) stateFn {
	switch r := l.next(); {
	case r == eof:
		return nil
	case r == '\n':
		l.emit(NL)
		return lexAny
	case r == ';':
		l.emit(Semi)
		return lexAny
	case isSpace(r):
		if l.peek() == '/' {
			l.next()
			return lexComment
		} else {
			l.emit(Space)

		}
		return lexAny
	case r == '"':
		return lexQuote
	case r == '`':
		return lexSym
	case r == '-':
		if l.peek() == ':' {
			l.next()
			return lexOperator
		}
		if l.start > 0 {
			rr, _ := utf8.DecodeLastRuneInString(l.input[:l.start])
			if isAlphaNum(rr) || rr == ')' || rr == ']' {
				return lexOperator
			}
		}
		fallthrough
	case r == '.' || isDigit(r):
		l.backup()
		return lexNumber
	case r == ':':
		if l.peek() == ':' {
			l.next()
			l.emit(Oper)
		} else {
			l.emit(Assign)
		}
		return lexAny
	case r == '/' && l.start == 0:
		return lexComment
	case l.isOper(r):
		return lexOperator
	case isAlphaNum(r):
		l.backup()
		return lexIdentifier
	case r == '[':
		l.emit(LeftBrack)
		return lexAny
	case r == ']':
		l.emit(RightBrack)
		return lexAny
	case r == '(':
		l.emit(LeftParen)
		return lexAny
	case r == ')':
		l.emit(RightParen)
		return lexAny
	case r == '{':
		l.emit(LeftCurly)
		return lexAny
	case r == '}':
		l.emit(RightCurly)
		return lexAny
	case r <= unicode.MaxASCII && unicode.IsPrint(r):
		l.emit(Char)
		return lexAny
	default:
		return l.errorf("unrecognised char:%U", r)
	}
}

// comment marker is behind
func lexComment(l *Scanner) stateFn {
	for {
		r := l.next()
		if r == eof || r == '\n' {
			l.backup()
			l.emit(Comment)
			break
		}
	}
	return lexAny
}

func lexIdentifier(l *Scanner) stateFn {
	for {
		switch r := l.next(); {
		case isAlphaNum(r), r == '.':
		// proceed
		default:
			l.backup()
			word := l.input[l.start:l.pos]
			if isAllDigits(word) {
				l.emit(Number)
				return lexAny
			} else {
				l.emit(Identifier)
				return lexAny
			}

		}
	}
}

func lexOperator(l *Scanner) stateFn {
	l.emit(Oper)
	return lexAny
}

func lexQuote(l *Scanner) stateFn {
	for {
		switch l.next() {
		case '\\':
			if r := l.next(); r != eof && r != '\n' {
				break
			}
			fallthrough
		case eof, '\n':
			return l.errorf("unterminated string")
		case '"':
			l.emit(String)
			return lexAny
		}
	}
}

func lexSym(l *Scanner) stateFn {
	r := l.next()
	// check if not standalone sym
	if r != '.' && !isLetterOrDigit(r) && r != ':' && r != '`' {
		l.backup()
		l.emit(Sym)
		return lexAny
	}
	for {
		r := l.next()
		if !isAlphaNum(r) && r != '.' && r != '`' && r != ':' {
			l.backup()
			l.emit(Sym)
			return lexAny
		}
	}
}

func lexTemporal(l *Scanner) stateFn {
	// [0-9]+Dhh[:mm[:ss[.S{1-9}]]
	// yyyy.mm.ddDhh[:mm[:ss[.S{1-9}]]
	// yyyy.mm.ddThh[:mm[:ss[.SSS]]]
	// yyyy.mm.dd
	// yyyy.mm
	// hh:mm:ss

	rl := l.acceptRun(digits)
	switch rl {
	case 0:
		l.emit(Temporal)
	case 2:
		if l.accept(".:") {
			return lexTemporal
		} else {
			l.emit(Temporal)
		}
	case 4:
		if l.accept(".") {
			return lexTemporal
		}
		fallthrough
	default:
		if l.accept("D") {
			if !isDigit(l.peek()) {
				l.emit(Temporal)
				return lexAny
			}
			return lexTemporal

		}
	}
	return lexAny
}

func lexNumber(l *Scanner) stateFn {

	if l.accept("012") {
		r := l.peek()
		if r == ':' {
			l.next()
			if l.peek() == ':' {
				l.next()
			}
			return lexOperator
		}
		l.backup()
	}

	if l.accept("0") {
		tLetters := "ghjefpmdnznuvt"
		r := l.peek()
		if r == 'N' || r == 'n' {
			l.next()
			l.accept(tLetters)
			l.emit(Number)
			return lexAny
		} else if r == 'w' || r == 'W' {
			l.next()
			l.accept(tLetters[1:])
			l.emit(Number)
			return lexAny
		}
		l.backup()
	}

	if l.accept("-") {
		// check what is after - could be some other op
		r := l.peek()
		if r == ':' {
			l.next()
			return lexOperator
		} else if r != '.' && !isDigit(r) {
			l.emit(Oper)
			return lexAny
		}
	}

	if l.accept(".") {
		r := l.peek()
		if r == ':' {
			l.next()
			return lexOperator
		}
		if !isDigit(r) {
			return lexIdentifier
		}
	}

	if l.accept("0") && l.accept("x") {
		l.acceptRun(hexDigits)
		l.emit(Number)
		return lexAny
	}

	l.acceptRun(digits)

	if l.accept("bhijpnuvtf") {
		if isLetterOrDigit(l.peek()) {
			return l.errorf("bad number")
		}
	}

	if l.accept("D:") {
		l.rewind()
		return lexTemporal
	}

	if l.accept(".") {
		l.acceptRun(digits)
	}

	if l.accept(".") {
		l.rewind()
		return lexTemporal
	}
	if l.accept("e") {
		l.accept("+-")
		l.acceptRun(digits)
	}
	l.emit(Number)
	return lexAny
}

var digits = "0123456789"
var hexDigits = digits + "abcdefABCDEF"

func isDigit(r rune) bool {
	return '0' <= r && r <= '9'
}
func isAlphaNum(r rune) bool {
	return r == '_' || isLetterOrDigit(r)
}

func isLetterOrDigit(r rune) bool {
	return unicode.IsLetter(r) || isDigit(r)
}

func isSpace(r rune) bool {
	return r == ' ' || r == '\t'
}

func isAllDigits(s string) bool {
	for _, c := range s {
		if !isDigit(c) {
			return false
		}
	}
	return true
}

func (l *Scanner) isOper(r rune) bool {
	switch r {
	case ',', '_', '+', '-', '%', '*', '!', '@', '^', '~', '#', '$', '/', '?', '.', '&', '|', '\\', '=', '\'':
		if l.peek() == ':' {
			l.next()
		}
	case '>':
		switch l.peek() {
		case '=', ':':
			l.next()
		}
	case '<':
		switch l.peek() {
		case '=', '>', ':':
			l.next()
		}
	default:
		return false
	}
	return true
}
