package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
)

func printVal(e expr) {
	fmt.Printf("%v\n", e)
}

func eval(e []expr) expr {
	if len(e) == 0 {
		return expr{}
	}
	return e[0]
}

type expr struct {
	val string
	t   string
}

func rexpr(r io.ByteReader) []expr {
	var elist = make([]expr, 0)

	sc := NewScanner(r)
	tk := sc.Next()
	for tk.Type != EOF {
		fmt.Printf("%s\n", tk.Text)
		tk = sc.Next()
	}

	return elist
}

func main() {
	flag.Parse()
	if flag.NArg() > 0 {
		for i := 0; i < flag.NArg(); i++ {
			name := flag.Arg(i)
			var f io.Reader
			var err error
			f, err = os.Open(name)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err)
			}
			rexpr(bufio.NewReader(f))
		}
	}

	rIn := bufio.NewReader(os.Stdin)
	for {
		printVal(eval(rexpr(rIn)))
	}
}
