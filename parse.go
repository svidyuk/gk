package main

type Parser struct {
	s      *Scanner
	errCnt int
	peekTk Token
	curTk  Token
}

type Expr interface {
	Prog() string
	Eval() Value
}

type Value interface {
	String() string
	Eval() Value
	Prog() string
}

func NewParser(sc *Scanner) *Parser {
	return &Parser{
		s: sc,
	}
}

func (p *Parser) peek() Token {
	tok := p.peekTk
	if tok.Type != EOF {
		return tok
	}
	p.peekTk = p.s.Next()
	return p.peekTk
}
func (p *Parser) next() Token {
	tok := p.peekTk
	if tok.Type != EOF {
		p.peekTk = Token{Type: EOF}
	} else {
		tok = p.s.Next()
	}
	if tok.Type == Error {
		p.errorf("%q", tok)
	}
	p.curTk = tok

	return tok
}

func (p *Parser) errorf(format string, args ...interface{}) {

}

// line:
// exprlist\n
func (p *Parser) Line() ([]Expr, bool) {
	//tok := p.peek()
	exprs, ok := p.exprList()
	if !ok {
		return nil, false
	}
	return exprs, true
}

// exprlist:
// \n
// stlist \n
func (p *Parser) exprList() ([]Expr, bool) {
	tok := p.next()
	switch tok.Type {
	case EOF:
		return nil, false
	case NL:
		return nil, true
	}
	exprs, ok := p.stList(tok)
	if !ok {
		return nil, false
	}
	tok = p.next()
	switch tok.Type {
	case EOF, NL:
	default:
		p.errorf("unexpected %s", tok)
	}
	return exprs, ok
}

// stlist:
// 	st
// 	st;st
func (p *Parser) stList(tok Token) ([]Expr, bool) {
	expr, ok := p.statement(tok)
	if !ok {
		return nil, false
	}
	var exprs []Expr
	if expr != nil {
		exprs = []Expr{expr}
	}
	if p.peek().Type == Semi {
		p.next()
		more, ok := p.stList(p.next())
		if ok {
			exprs = append(exprs, more...)
		}
	}
	return exprs, true
}

// st:
//	var:Expr
//	Expr
func (p *Parser) statement(tok Token) (Expr, bool) {
	varName := ""
	if tok.Type == Identifier {
		if p.peek().Type == Assign {
			p.next()
			varName = tok.Text
			tok = p.next()
		}
	}
	expr := p.expr(tok)
	if expr == nil {
		return nil, true
	}
	if varName != "" {
		//expr = &assignment{v: p.variable(varName), e: expr}
	}
	return expr, true
}

// expr
//	operand
//	operand binop expr
func (p *Parser) expr(tok Token) Expr {
	return nil
}

// operand:
//		number
//		char constant
//		string constant
//		symbol
//		vector
//		operand [Expr]
//		unary Expr
func (p *Parser) operand(t Token, idxOK bool) Expr {
	return nil
}

// index
//	expr
// 	expr [stlist]
//	expr [stlist][stlist]...
func (p *Parser) index(expr Expr) Expr {
	return nil
}

// number
//	integer
//	float
//	'(' Expr ')'
func (p *Parser) number(tok Token) Expr {
	return nil
}

// function def
//	{ explist}
// {[stList] explist}
func (p *Parser) function(tok Token) {

}
